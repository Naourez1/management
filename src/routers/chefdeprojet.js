var express = require("express");

var db = require("../models/db");

var chefdeprojetmodel = require("../models/chefdeprojetmodel");
var listedeprojetmodel = require("../models/listedeprojetmodel");

var router = express.Router();
/* router.use(express.static(path.join(__dirname,'uploads')));
var storage = multer.diskStorage({
  destination: function(req, file, cb){
    cb(null,'./src/uploads/')
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});
var upload = multer ({storage : storage});
router.get('/getimage',function(req, res) {
  res.sendFile("C:/Users/HP/Downloads/app-naourez/src/uploads/"+req.query.img)
}) */

router.get("/", function (req, res) {
  res.send("Hello chefdeprojet")

});


router.get("/add", function (req, res) {
  var adm = new chefdeprojetmodel({
    firstname: req.query.firstname,
    lastname: req.query.lastname,
    mail: req.query.mail,
    password: req.query.password,
    role: "chefdeprojet",
    position: req.query.position,
    status: req.query.status,
    activity: req.query.activity
  });
  adm.save(function (err) {
    if (err) {
      res.send({err: "there are an error"});
      throw err
    }
    res.send({add: true})

  })

});
router.get("/update", function (req, res) {
  chefdeprojetmodel.findByIdAndUpdate(req.query.id, {
    firstname: req.query.firstname,
    lastname: req.query.lastname,
    mail: req.query.mail,
    password: req.query.password,
    position: req.query.position,
    status: req.query.status,
    activity: req.query.activity

  }, {new: true}, function (err) {
    if (err) {
      res.send({err: "there are an error"});
      throw err
    }
    res.send({update: true})

  })

});
router.get("/findbyid", function (req, res) {
  chefdeprojetmodel.findById(req.query.id, function (err, chefdeprojet) {
    if (err) {
      res.send({err: "there are an error"});
      throw err
    }
    res.send(chefdeprojet)


  })

});
router.get("/all", function (req, res) {
  chefdeprojetmodel.find({}, function (err, result) {
    res.send(result)

  })

});
router.get("/remove", function (req, res) {
  chefdeprojetmodel.remove({_id: req.query.id}, function (err) {
    if (err) {
      res.send({err: "there are an error"});
      throw err
    }
    res.send({remove: true})
  })

});


/* router.post("/add", upload.single("image"),function(req, res) {
  var chef = new chefdeprojetmodel({
    firstname: req.query.firstname,
    lastname: res.query.lastname,
    mail: req.query.mail,
    password: req.query.password,
    photo : req.file.filename,
    tache:[],
    nomequipe: req.query.nomequipe,
    listedequggestions :[],
    listedesprojets:[],
    role: "chefdeprojet"
  });
chef.save(function(err){
  if(err){
    res.send({err: "there is an error"});
    throw err
  }
  res.send({add :true})
})
}); */
router.get("/ajouterprojet",function (req,res) {
  chefdeprojetmodel.findById(req.query.id,function (err,chef) {
    if (err){
      res.send({err:"there are an error"})
      throw err
    } else {
      var ta=new listedeprojetmodel({
        nomprojet:req.query.nomprojet
      });
      chef.listedesprojets.push(ta);
      chef.save(function (err) {
        if (err){
          res.send({err:"there are an error"})
          throw err
        } else {
          res.send({addprojet:true})
        }
      })
    }
  })
});


module.exports = router;
