var express = require("express");

var db = require("../models/db");

var managermodel = require("../models/managermodel");


var router = express.Router();


router.get("/", function (req, res) {
  res.send("Hello manager")

});

router.get("/add", function (req, res) {
  var adm = new managermodel({
    firstname: req.query.firstname,
    lastname: req.query.lastname,
    mail: req.query.mail,
    password: req.query.password,
    role: "manager"
  });
  adm.save(function (err) {
    if (err) {
      res.send({err: "there are an error"});
      throw err
    }
    res.send({add: true})

  })

});
router.get("/update", function (req, res) {
  managermodel.findByIdAndUpdate(req.query.id, {
    firstname: req.query.firstname,
    lastname: req.query.lastname,
    mail: req.query.mail,
    password: req.query.password
  }, {new: true}, function (err) {
    if (err) {
      res.send({err: "there are an error"});
      throw err
    }
    res.send({update: true})

  })

});
router.get("/findbyid", function (req, res) {
  managermodel.findById(req.query.id, function (err, manager) {
    if (err) {
      res.send({err: "there are an error"});
      throw err
    }
    res.send(manager)


  })

});
router.get("/all", function (req, res) {
  managermodel.find({}, function (err, result) {
    res.send(result)

  })

});
router.get("/remove", function (req, res) {
  managermodel.remove({_id: req.query.id}, function (err) {
    if (err) {
      res.send({err: "there are an error"});
      throw err
    }
    res.send({remove: true})
  })

});


module.exports = router;
