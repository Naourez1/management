var express = require("express");
var db = require("../models/db");
var adminmodel = require("../models/adminmodel");
var chefdeprojetmodel = require("../models/chefdeprojetmodel");
var managermodel = require("../models/managermodel");
var router = express.Router();


router.get("/", function (req, res) {
  res.send("Hello Admin")

});

router.get("/add", function (req, res) {
  var adm = new adminmodel({
    firstname: req.query.firstname,
    lastname: req.query.lastname,
    mail: req.query.mail,
    password: req.query.password,

    role: "admin"
  });
  adm.save(function (err) {
    if (err) {
      res.send({err: "there are an error"});
      throw err
    }
    res.send({add: true})

  })

});
router.get("/update", function (req, res) {
  adminmodel.findByIdAndUpdate(req.query.id, {
    firstname: req.query.firstname,
    lastname: req.query.lastname,
    mail: req.query.mail,
    password: req.query.password
  }, {new: true}, function (err) {
    if (err) {
      res.send({err: "there are an error"});
      throw err
    }
    res.send({update: true})

  })

});
router.get("/findbyid", function (req, res) {
  adminmodel.findById(req.query.id, function (err, admin) {
    if (err) {
      res.send({err: "there are an error"});
      throw err
    }
    res.send(admin)


  })

});
router.get("/all", function (req, res) {
  adminmodel.find({}, function (err, result) {
    res.send(result)

  })

});
router.get("/remove", function (req, res) {
  adminmodel.remove({_id: req.query.id}, function (err) {
    if (err) {
      res.send({err: "there are an error"});
      throw err
    }
    res.send({remove: true})
  })

});

router.get("/login", function (req, res) {
  adminmodel.find({mail: req.query.mail, password: req.query.password}, function (err, result) {
    if (err) {
      res.send({err: "there are an error"})
      throw err
    } else {
      if (result.length == 0) {
        managermodel.find({mail: req.query.mail, password: req.query.password}, function (err, result) {
          if (err) {
            res.send({err: "there are an error"})
            throw err
          } else {
            if (result.length == 0) {
              chefdeprojetmodel.find({mail: req.query.mail, password: req.query.password}, function (err, result) {
                if (err) {
                  res.send({err: "there are an error"})
                  throw err
                } else {
                  if (result.length == 0) {
                    res.send({auth: false})
                  } else {
                    res.send({auth: true, user: result[0], role: result[0].role})
                  }
                }

              })
            } else {
              res.send({auth: true, user: result[0], role: result[0].role})
            }
          }

        })

      } else {
        res.send({auth: true, user: result[0], role: result[0].role})

      }


    }

  })

})
module.exports = router;
