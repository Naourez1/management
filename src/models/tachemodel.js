
var mongoose = require('mongoose');
Schema = mongoose.Schema;
var tachemodelSchema=mongoose.Schema({
  title: {
    type:String
},
  start: {
    type:String
},
  end: {
    type:String
},
  description: {
    type:String
},
  estimation: { 
    type: String
},
  priority:{
        type:String
    },
  project_id: { type: mongoose.Schema.Types.ObjectId, ref: 'projetmodel' }
    
});
const tachemodel = module.exports = mongoose.model('Tache', tachemodelSchema);