var mongoose = require("mongoose");

var SChema = mongoose.Schema;

var adminmodelSChema = new SChema({
  firstname: String,
  lastname: String,
  mail: {type: String, unique: true},
  username: String,
  password: String,
  role: String,
});

var adminmodel = mongoose.model("adminmodel", adminmodelSChema);

module.exports = adminmodel;
