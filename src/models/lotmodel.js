var mongoose = require("mongoose");
const moment = require('moment');


var SChema = mongoose.Schema;

/* var tachemodelSChema = new SChema({
  title: String,
  start: Date,
  end: Date,
  prioriy : String,
  description: String,
  estimation: Date
}); */
var lotmodelSChema = new SChema({
  nomlot: {
    type:String
},
  datedebut: {
    type:String
},
  datefin: {
    type:String
},
  priority : {
    type:String
},
  description: {
    type:String
},
  estimation: {
    type:String
},
  tache_id:   { type: String },//clé etrangère

  project_id: { type: mongoose.Schema.Types.ObjectId, ref: 'projetmodel' } //clé etrangère

});

const lotmodel = module.exports = mongoose.model('lotmodel', lotmodelSChema);
