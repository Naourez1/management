var mongoose = require("mongoose");

var SChema = mongoose.Schema;
var tachemodelSChema = new SChema({
  title: String,
  start: Date,
  priority : String,
  end: Date,
  description: String,
  estimation: Date,
  totla: Number
});
var lotmodelSChema = new SChema({
  nomlot: String,
  datedebut: Date,
  datefin: Date,
  priority : String,
  description: String,
  estimation: Date,
  total: Number,
  listedetache: [tachemodelSChema]
});
var projetmodelSChema = new SChema({
  nomprojet: String,
  datedebut: Date,
  datefin: Date,
  nomclient: String,
  nomequipe: String,
  author: String,
  priority: String,
  task: String,
  listedelot: [lotmodelSChema]
});

var projetmodel = mongoose.model("projetmodel", projetmodelSChema);

module.exports = projetmodel;
