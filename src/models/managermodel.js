var mongoose = require("mongoose");

var SChema = mongoose.Schema;

var managermodelSChema = new SChema({
  firstname: String,
  lastname: String,
  mail: {type: String, unique: true},
  username: String,
  password: String,
  role: String,
});

var managermodel = mongoose.model("managermodel", managermodelSChema);

module.exports = managermodel;
