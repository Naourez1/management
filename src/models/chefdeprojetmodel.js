var mongoose = require("mongoose");

var SChema = mongoose.Schema;
var listedeprojetmodelSChema = new SChema({
  nomprojet: String
 });
var chefdeprojetmodelSChema = new SChema({
  firstname: String,
  lastname: String,
  mail: {type: String, unique: true},
  username: String,
  password: String,
  role: String,
  position: String,
  status: String,
  activity: String,
  listedeprojets: [listedeprojetmodelSChema]
});

var chefdeprojetmodel = mongoose.model("chefdeprojetmodel", chefdeprojetmodelSChema);

module.exports = chefdeprojetmodel;
