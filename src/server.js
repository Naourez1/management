var express = require("express");
// var moment = require('moment');
// moment().format('yyyy-mm-dd');
var cors = require("cors");

var adm = require("./routers/admin");
var mng = require("./routers/manager");
var chef = require("./routers/chefdeprojet");
var pro = require("./routers/projet");
var app = express();
var getTitleAtUrl = require('get-title-at-url');

/* const scrape = require('website-scraper');

let options = {
    urls: ['https://ecd9dc861e.lite.easyproject.com/issues/new?utm_campaign=menu&utm_content=easy_new_entity&utm_term=issues_new/'],
    directory: './issue',
};

scrape(options).then((result) => {
    console.log("Website succesfully downloaded");
}).catch((err) => {
    console.log("An error ocurred", err);
}); */

app.use(cors());

app.use("/admin", adm);
app.use("/manager", mng);
app.use("/chefdeprojet", chef);
app.use("/projet", pro);

app.get("/", function (req, res) {
  res.send("okkey")
});

app.listen(3000, function () {
  console.log("start")
});
