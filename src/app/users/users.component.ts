import { Component, OnInit } from '@angular/core';
import { RestService } from '../services/rest.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
chefs: any;
managers: any;
  constructor(public rest: RestService) { }

  ngOnInit() {
    this.rest.getallchef().subscribe((res) => {
      console.log(res);
      this.chefs = res});
    this.rest.getallmanager().subscribe((res) => {
        console.log(res);
        this.managers = res});
  }

}
