import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(projets: any[], term: string): any {
    if(term === undefined)return projets;
      return projets.filter(function(projet){
      return projet.nomprojet.toLowerCase().includes(term.toLowerCase());
    })
  }

}
