import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PBComponent } from './pb.component';

describe('PBComponent', () => {
  let component: PBComponent;
  let fixture: ComponentFixture<PBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
