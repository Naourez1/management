import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { RestService } from '../services/rest.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Projet } from '../services/projetmodal';
import { Tache } from '../services/tache';
import { TacheComponent } from '../tache/tache.component';

@Component({
  selector: 'app-sb',
  templateUrl: './sb.component.html',
  styleUrls: ['./sb.component.css']
})
export class SBComponent implements OnInit {
  todo = [
    'Get to work',
    'Pick up groceries',
    'Go home',
    'Fall asleep'
  ];
  done = [
    'Get up',
    'Brush teeth',
    'Take a shower',
    'Check e-mail',
    'Walk dog'
  ];

  constructor(public rest: RestService) {
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }
/*   getalltache(){
    this.rest.getalltache(this.id, this.idc).subscribe(res => {

    this.tasks = res
    console.log(this.tasks)
  }
      );
  } */

  ngOnInit() {

  }
}
