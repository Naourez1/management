import { Component, OnInit } from '@angular/core';
import { UIChart } from 'primeng/chart';

@Component({
  selector: 'app-monitoring',
  templateUrl: './monitoring.component.html',
  styleUrls: ['./monitoring.component.css']
})
export class MonitoringComponent implements OnInit {
  data: any;
  options: any;
    constructor() {
      this.options = {
        title: {
            display: true,
            text: 'My Title',
            fontSize: 16
        },
        legend: {
            position: 'bottom'
        }
    };
      this.data = {
        labels: ['A','B','C'],
        datasets: [
            {
                data: [300, 50, 100],
                backgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56"
                ],
                hoverBackgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56"
                ]
            }]    
        };
    }

    /* update(chart: UIChart) {
        this.data = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
                {
                    label: 'Dataset',
                    data: [70, 60, 50, 40, 30, 20, 10],
                    fill: false,
                    borderColor: '#4bc0c0'
                }
            ]
        };

        setTimeout(() => {
          chart.refresh();
       }, 100);    } */

  ngOnInit() {
  }

}
