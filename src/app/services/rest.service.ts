import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(public http: HttpClient) {
  }

  login(mail, password) {
    return this.http.get('http://localhost:3000/admin/Login?mail=' + mail + '&password=' + password);

  }
filterProject(name: string ) {
  return this.http.get('http://localhost:3000/projet/filterProjet?author=' + name);
}
  addmanager(u) {
    return this.http.get('http://localhost:3000/manager/add?firstname=' + u.firstname
      + '&lastname=' + u.lastname
      + '&mail=' + u.mail
      + '&password='
      + u.password
      + '&status='
      + u.status
      + '&position='
      + u.position
      + '&activity='
      + u.activity);
  }

  getallmanager() {
    return this.http.get('http://localhost:3000/manager/all');
  }

  removemanager(id) {
    return this.http.get('http://localhost:3000/manager/remove?id=' + id);
  }

  updatemanager(u) {
    return this.http.get('http://localhost:3000/manager/update?id=' + u._id + '&firstname='
      + u.firstname + '&lastname='
      + u.lastname + '&mail='
      + u.mail + '&password='
      + u.password
      + '&status='
      + u.status
      + '&position='
      + u.position
      + '&activity='
      + u.activity);
  }

  addchef(u) {
    return this.http.get('http://localhost:3000/chefdeprojet/add?firstname=' + u.firstname
      + '&lastname=' + u.lastname
      + '&mail=' + u.mail
      + '&password='
      + u.password
      + '&status='
      + u.status
      + '&position='
      + u.position
      + '&activity='
      + u.activity);
  }

  getallchef() {
    return this.http.get('http://localhost:3000/chefdeprojet/all');
  }

  removechef(id) {
    return this.http.get('http://localhost:3000/chefdeprojet/remove?id=' + id);
  }

  updatechef(u) {
    return this.http.get('http://localhost:3000/chefdeprojet/update?id=' + u._id + '&firstname='
      + u.firstname + '&lastname='
      + u.lastname + '&mail='
      + u.mail + '&password='
      + u.password + '&status='
      + u.status
      + '&position='
      + u.position
      + '&activity='
      + u.activity);
  }

  ajouterprojet(u) {
    return this.http.get('http://localhost:3000/projet/add?nomprojet='
      + u.nomprojet + '&datedebut='
      + u.datedebut + '&datefin='
      + u.datefin + '&nomclient='
      + u.nomclient + '&nomequipe='
      + u.nomequipe + '&author='
      + u.author + '&priority='
      + u.priority + '&task='
      + u.task);

  }

  getallprojet() {
    return this.http.get('http://localhost:3000/projet/all');
  }

  removeprojet(id) {
    return this.http.get('http://localhost:3000/projet/remove?id=' + id);
  }

  updateprojet(u) {
    return this.http.get('http://localhost:3000/projet/update?id='
      + u._id + '&nomprojet='
      + u.nomprojet + '&datedebut='
      + u.datedebut + '&datefin='
      + u.datefin + '&nomclient='
      + u.nomclient + '&nomequipe='
      + u.nomequipe + '&author='
      + u.author + '&priority='
      + u.priority + '&task='
      + u.task);

  }

  findbyidlot(id, u) {
    return this.http.get('http://localhost:3000/projet/findbyidlot?id=' + id + '&idc=' + u._id);
  }

  getalllot(id) {
    return this.http.get('http://localhost:3000/projet/getalllot?id=' + id);
  }

  ajouterlot(id, u) {
    return this.http.get('http://localhost:3000/Projet/ajouterlot?id='
      + id + '&nomlot='
      + u.nomlot + '&datedebut='
      + u.datedebut + '&datefin='
      + u.datefin );
  }

  removelot(id, idc) {
    return this.http.get('http://localhost:3000/Projet/supprimerlot?id=' + id + '&idc=' + idc);
  }

  updatelot(id, u) {
    return this.http.get('http://localhost:3000/Projet/updatelot?id='
      + id + '&idc='
      + u._id + '&nomlot='
      + u.nomlot + '&datedebut='
      + u.datedebut + '&datefin='
      + u.datefin );
  }

  ajoutertache(id, idc, u) {
    return this.http.get('http://localhost:3000/projet/ajoutertache?id='
      + id + '&idc='
      + idc + '&title='
      + u.title + '&start='
      + u.start + '&end='
      + u.end );
  }

  removetache(id, idc, idcc) {
    return this.http.get('http://localhost:3000/projet/supprimertache?id=' + id + '&idc=' + idc + '&idcc=' + idcc);
  }

  updatetache(id, idc, u) {
    return this.http.get('http://localhost:3000/projet/updatetache?id='
      + id + '&idc='
      + idc + '&idcc='
      + u._id + '&title='
      + u.title + '&start='
      + u.start + '&end='
      + u.end );
  }

  getalltache(id, idc) {
    return this.http.get('http://localhost:3000/projet/getalltache?id=' + id + '&idc=' + idc);
  }

  findbyname(nomequipe) {
    return this.http.get('http://localhost:3000/projet/findbyname?nomequipe=' + nomequipe);
  }
  findbyid(u) {
    return this.http.get('http://localhost:3000/projet/findbyid?id=' + u._id);
  }
  affecterprojet(id, nomprojet) {
    return this.http.get('http://localhost:3000/chefdeprojet/ajouterporjet?id=' + id + '&nomprojet=' + nomprojet );
  }
  findbyidprojlot(id) {
    return this.http.get('http://localhost:3000/projet/' + id + '/getlotbyidproj/' );
  }
  findbyidprojtache(id) {
    return this.http.get('http://localhost:3000/projet/' + id + '/gettaskbyidproj/' );
  }
  ajoutertache1(id, u) {
    return this.http.get('http://localhost:3000/projet/' + id + '/addtache?title=' + u.title
    + '&start=' + u.start
    + '&end=' + u.end
    + '&description=' + u.description
    + '&estimation=' + u.estimation
    + '&priority=' + u.priority );
  }
  ajouterlot1(id, u) {
    return this.http.get('http://localhost:3000/projet/' + id + '/addlot?nomlot=' + u.nomlot
    + '&datedebut=' + u.datedebut
    + '&datefin=' + u.datefin
    + '&description=' + u.description
    + '&estimation=' + u.estimation
    + '&priority=' + u.priority
    + '&tache_id=' + u.tache_id );
  }
}
