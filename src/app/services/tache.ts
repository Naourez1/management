export class Tache {
    title: string;
    start: Date;
    priority: string;
    end: Date;
    description: string;
    estimation: Date;
}
