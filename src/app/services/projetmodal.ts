import {Lotmodal} from '../services/lotmodel';
export class Projet {
  nomprojet: string;
  datedebut: Date;
  datefin: Date;
  nomclient: string;
  nomequipe: string;
  description: string;
  author: string;
  priority: string;
  task: string;
  listedelot: Lotmodal[];
  status: string;
}
