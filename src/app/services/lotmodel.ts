import {Tache} from '../services/tache';
export class Lotmodal {
    nomlot: string;
    datedebut: Date;
    datefin: Date;
    priority : string;
    description: string;
    estimation: Date;
    listedetache: Tache[];
}
