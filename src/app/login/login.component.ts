import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RestService} from '../services/rest.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  mail: string;
  password: string;

  constructor(public router: Router, public rest: RestService) {
  }

  ngOnInit() {
  }

  login(mail, password) {
    this.rest.login(mail, password).subscribe(auth => {
      console.log(auth);
      if (auth['auth'] === false) {
        this.mail = '';
        this.password = '';
      } else {
        localStorage.setItem('id', auth['user']['_id']);
        localStorage.setItem('role', auth['role']);
        localStorage.setItem('user', JSON.stringify(auth['user']));
        this.router.navigate(['home/open']);
      }

    });
  }
}
