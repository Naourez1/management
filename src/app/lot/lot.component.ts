import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {RestService} from '../services/rest.service';

@Component({
  selector: 'app-lot',
  templateUrl: './lot.component.html',
  styleUrls: ['./lot.component.css']
})
export class LotComponent implements OnInit, OnDestroy {
  id: number;
  private sub: any;
  lots: any;
  role: any;
  nomprojet: any;
  result: any;
  es: any;
  lot: any = {
    _id: '',
    nomlot: '',
    datedebut: '',
    datefin: '',
    priority: '',
    description: '',
    estimation: '',
    tache_id: ''

  };

  constructor(public route: ActivatedRoute, public rest: RestService, public http: HttpClient, public router: Router) {
    this.nomprojet = JSON.parse(localStorage.getItem('resultat')).nomprojet;
    this.role = localStorage.getItem('role');


  }
 findbydate(datedebut , datefin) {
   return datefin - datedebut;
 }

 addlot(id, u) {
  this.rest.ajouterlot1(id, u).subscribe(res => {
    console.log('add');
    this.ngOnInit();
    this.lot = {
      _id: '',
      nomlot: '',
      datedebut: '',
      datefin: '',
      priority: '',
      estimation: '',
      description: '',
      tache_id: ''
    };
  });
}

  edit(r) {
    this.lot = {

      _id: r._id,
      nomlot: r.nomlot,
      datedebut: r.datedebut,
      datefin: r.datefin,
      priority: r.priority,
      estimation: r.estimation,
      description: r.description,
      tache_id: r.tache_id
    };


  }


  remove(id, idc) {

    this.rest.removelot(id, idc).subscribe();
    this.ngOnInit();

  }

  updatelot(id, u) {
    this.rest.updatelot(id, u).subscribe(res => {
      console.log('done');
      this.ngOnInit();
      this.lot = {
        _id: '',
        nomlot: '',
        datedebut: '',
        datefin: '',
        priority: '',
        estimation: '',
        description: '',
        tache_id : ''
      };
    });

  }
  find(id, u) {
    this.rest.findbyidlot(id, u).subscribe(auth => {
      localStorage.setItem('identifiantlot', auth['resultatlot']['_id']);
      localStorage.setItem('resultatlot', JSON.stringify(auth['resultatlot']));
      this.router.navigate(['home/configtache', id, u._id]);
    });
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {

      this.id = params['id']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
    });
    this.rest.getalllot(this.id).subscribe(res => this.lots = res);

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
