import { Component, OnInit, ViewChild } from '@angular/core';
import { RestService } from '../services/rest.service';
import { MatTableDataSource, MatSort, MatPaginator, MatTable } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { NotificationService } from '../services/notification.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-configurationprojet',
  templateUrl: './configurationprojet.component.html',
  styleUrls: ['./configurationprojet.component.scss']
})
export class ConfigurationprojetComponent implements OnInit {

  constructor(private titleService: Title, public rest: RestService, public http: HttpClient, public router: Router,private dialog:
     MatDialog, private notificationService: NotificationService ) {
      this.role = localStorage.getItem('role');
      this.id = localStorage.getItem('id');
      this.nomequipe = JSON.parse( localStorage.getItem('user')).nomequipe;
      this.getprojet();
     }
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['nomprojet', 'datedebut', 'datefin', 'nomclient', 'nomequipe', 'author',
   'priority', 'modules', 'actions1', 'actions2'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('table') table: MatTable<Element>;
  searchKey: string;
  date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());
  id: any;
  projets: any;
  projetss: any;
  projetsss: any;
  projetssss: any;
  listedeprojet: any;
  role: any;
  nomequipe: any;
  nomprojet: any;
  identifiant: any;
  projet: any = {
    _id: '',
    nomprojet: '',
    datedebut: '',
    datefin: '',
    nomclient: '',
    nomequipe: '',
    task: '',
    priority: '',
    author: ''
  };
  priorities = [
    { id: 1, value: 'High' },
    { id: 2, value: 'Normal' },
    { id: 3, value: 'Strategic' }];


    tasks = [
      { id: 1, value: 'Task' }];

      form: FormGroup;

  ngOnInit() {
    this.form = new FormGroup({
      id: new FormControl(null),
      nomprojet: new FormControl('', Validators.required),
      nomclient: new FormControl('', Validators.required),
      nomequipe: new FormControl('', Validators.required),
      author: new FormControl(''),
      priority: new FormControl(0),
      task: new FormControl(0),
      datedebut: new FormControl(''),
      datefin: new FormControl(''),
    });
  }
  initializeFormGroup() {
    this.form.setValue({
      id: null,
      datedebut: '',
      datefin: '',
      nomclient: '',
      nomequipe: '',
      task: 0,
      priority: 0,
      author: ''
      });
  }
  onSubmit() {
    if (this.form.valid) {
      if (!this.form.get('id').value) {
      this.rest.ajouterprojet(this.form.value);
    } else {
      this.rest.updateprojet(this.form.value);
      this.form.reset();
      this.initializeFormGroup();
      this.notificationService.success(':: Submitted successfully');
    }
    }
  }
  onClear() {
    this.form.reset();
    this.initializeFormGroup();
    this.notificationService.success(':: Submitted successfully');
  }
 onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  getprojet() {
    this.rest.getallprojet().subscribe(res => this.projets = res);
}
edit(r) {
  this.projet = {
    _id: r._id,
    nomprojet: r.nomprojet,
    datedebut: r.datedebut,
    datefin: r.datefin,
    nomclient: r.nomclient,
    nomequipe: r.nomequipe,
    task: r.task,
    priority: r.priority,
    author : r.author
  };


}

  addprojet(u) {
    this.rest.ajouterprojet(u).subscribe(res => {
      console.log('add');
      this.getprojet();
      this.projet = {
        _id: '',
        nomprojet: '',
        datedebut: '',
        datefin: '',
        nomclient: '',
        nomequipe: '',
        task: '',
        priority: '',
        author: ''
      };
      console.log(this.projet);
    });
    this.table.renderRows();
    this.notificationService.success(':: Submitted successfully');

      }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }
  find(u) {
    this.rest.findbyid(u).subscribe(auth => {
      console.log(auth);
      /* tslint:disable:no-string-literal */
      localStorage.setItem('identifiant', auth['resultat']['_id']);
      /* tslint:enable:no-string-literal */
            /* tslint:disable:no-string-literal */
      localStorage.setItem('resultat', JSON.stringify(auth['resultat']));
      /* tslint:enable:no-string-literal */
      this.router.navigate(['home/configlot', u._id]);
    });
  }
  updateprojet(u) {
    this.rest.updateprojet(u).subscribe(res => {
      console.log('done');
      this.getprojet();
      this.projet = {
        _id: '',
        nomprojet: '',
        datedebut: '',
        datefin: '',
        nomclient: '',
        nomequipe: '',
        task: '',
        priority: '',
        author: ''
      };
    });
  }
  remove(id) {

    this.rest.removeprojet(id).subscribe();
    this.getprojet();
    this.notificationService.success(':: Deleted successfully');

  }
}
