import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {RestService} from '../services/rest.service';

@Component({
  selector: 'app-gestiondemanager',
  templateUrl: './gestiondemanager.component.html',
  styleUrls: ['./gestiondemanager.component.css']
})
export class GestiondemanagerComponent implements OnInit {
title: string;
  managers: any;
  manager: any = {
    _id: '',
    firstname: '',
    lastname: '',
    mail: '',
    password: ''

  };


  constructor(public rest: RestService, public http: HttpClient, public router: Router) {
    this.getmanager();

  }

  getmanager() {
    this.rest.getallmanager().subscribe(res => this.managers = res);

  }

  addmanager(u) {

    this.rest.addmanager(u).subscribe(res => {
      console.log('add');
      this.getmanager();
      this.manager = {
        _id: '',
        firstname: '',
        lastname: '',
        mail: '',
        password: '',
      };
    })

    ;
  }


  edit(r) {
    this.manager = {

      _id: r._id,
      firstname: r.firstname,
      lastname: r.lastname,
      mail: r.mail,
      password: r.password
    };


  }

  remove(id) {

    this.rest.removemanager(id).subscribe();
    this.getmanager();

  }

  updatemanager(u) {


    this.rest.updatemanager(u).subscribe(res => {
      console.log('done');
      this.getmanager();


      this.manager = {
        _id: '',
        firstname: '',
        lastname: '',
        mail: '',
        password: '',
      };
    });

  }


  ngOnInit() {
  }
}
