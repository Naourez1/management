import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestiondemanagerComponent } from './gestiondemanager.component';

describe('GestiondemanagerComponent', () => {
  let component: GestiondemanagerComponent;
  let fixture: ComponentFixture<GestiondemanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestiondemanagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestiondemanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
