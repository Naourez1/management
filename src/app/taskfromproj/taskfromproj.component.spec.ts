import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskfromprojComponent } from './taskfromproj.component';

describe('TaskfromprojComponent', () => {
  let component: TaskfromprojComponent;
  let fixture: ComponentFixture<TaskfromprojComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskfromprojComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskfromprojComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
