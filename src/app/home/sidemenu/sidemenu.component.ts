import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, } from '@angular/forms';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css']
})
export class SidemenuComponent implements OnInit {
  role: any;
  id: any;
  firstname: any;
  lastname: any;
  options: FormGroup;

  constructor() {
    this.role = localStorage.getItem('role');
    this.id = localStorage.getItem('id');
    console.log(JSON.parse(localStorage.getItem('user')).firstname);
    this.firstname = JSON.parse( localStorage.getItem('user')).firstname;
    this.lastname = JSON.parse( localStorage.getItem('user')).lastname;
  }

  ngOnInit() {
  }

}
