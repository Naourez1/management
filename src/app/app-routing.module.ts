import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LotComponent} from './lot/lot.component';
import {HomeComponent} from './home/home.component';
import {GestiondemanagerComponent} from './gestiondemanager/gestiondemanager.component';
import {ConfigurationprojetComponent} from './configurationprojet/configurationprojet.component';
import {TacheComponent} from './tache/tache.component';
import {LoginComponent} from './login/login.component';
import {GestionDeChefDeProjetComponent} from './gestion-de-chef-de-projet/gestion-de-chef-de-projet.component';
import { OpenComponent } from './open/open.component';
import { MonitoringComponent } from './monitoring/monitoring.component';
import { PBComponent } from './pb/pb.component';
import { SBComponent } from './sb/sb.component';
import { MeetingsComponent } from './meetings/meetings.component';
import { RegisterComponent } from './register/register.component';
import { DetailsComponent } from './details/details.component';
import { OpenedprojectsComponent } from './openedprojects/openedprojects.component';
import { UsersComponent } from './users/users.component';
import { TaskfromprojComponent } from './taskfromproj/taskfromproj.component';
import { TaskfromsprintComponent } from './taskfromsprint/taskfromsprint.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'home', component: HomeComponent, children: [
      {path: 'gestionchefdeprojet', component: GestionDeChefDeProjetComponent, data: { title: 'Project Managers' } },
      {path: 'gestionmanager', component: GestiondemanagerComponent , data: { title: 'Managers' } },
      {path: 'configprojet', component: ConfigurationprojetComponent , data: { title: 'Projects' } },
      {path: 'configlot/:id', component: LotComponent , data: { title: 'Modules' } },
      {path: 'configtache/:idP/:idL', component: TacheComponent , data: { title: 'Tasks' } },
      {path: 'open', component: OpenComponent , data: { title: 'Home ' }},
      {path: 'monitoring', component: MonitoringComponent, data: { title: 'Monitoring' }},
      {path: 'meeting', component: MeetingsComponent, data: { title: 'New Meeting' }},
      {path: 'pbs', component: PBComponent, data: { title: 'Tasks Monitoring' }},
      {path: 'sbs', component: SBComponent, data: { title: 'Modules Monitoring' }},
      {path: 'details', component: DetailsComponent, data: { title: 'Details' }},
      {path: 'openedpro', component: OpenedprojectsComponent, data: { title: 'Opened Projects' }},
      {path: 'users', component: UsersComponent, data: { title: 'users' }},
      {path: 'tp/:id', component: TaskfromprojComponent, data: { title: 'tp' }},
      {path: 'ts', component: TaskfromsprintComponent, data: { title: 'ts' }},





    ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
